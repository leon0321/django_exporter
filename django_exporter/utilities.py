import re

ALPHABETIC = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

FACTOR = 26

DELAY = 1

REGEX_NUMBER = r'\d+'


class DictObject(object):
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)


def get_dict_fields(dict_fields):
    dict_field_out = {}
    for field_name, column in dict_fields.items():
        try:
            column = int(column)
        except ValueError:
            if re.findall(REGEX_NUMBER, column):
                raise ValueError(
                    'You can\'t mix letters and numbers in the same column. you pass: {}'.format(column))
        if str == type(column):
            dict_field_out[field_name] = convert_alphabetic_column_to_number(column)
        elif int == type(column):
            dict_field_out[field_name] = column
        else:
            raise ValueError(
                'Column must be [aA - zZ]+ or [1 - 9]+,you pass \{"{0}":{1}\}'.format(field_name, column))
    return dict_field_out


def convert_alphabetic_column_to_number(alphabetic_column):
    number_list = map(convert_letter_to_number, list(alphabetic_column))
    return convert_list_number_to_decimal_integer(number_list)


def convert_list_number_to_decimal_integer(list_number):
    list_number = [item for item in list_number]
    list_number_reversed = list(reversed(list_number))
    final_number = 0
    for number, exp in zip(list_number_reversed, range(len(list_number_reversed))):
        final_number += (number + DELAY) * (FACTOR ** exp)
    return final_number - DELAY


def convert_letter_to_number(letter):
    try:
        return ALPHABETIC.index(letter.upper())
    except ValueError as ve:
        raise ValueError(ve.message + '[{0}] on ALPHABETIC = {1}'.format(letter.upper(), ALPHABETIC))


def merge(*args):
    list_dict = []
    merge_dict = {}
    for d in args:
        if not isinstance(d, dict):
            raise ValueError(u'object {} is not a dict instance'.format(d))
        list_dict += d.items()
    for key, value in list_dict:
        merge_dict[key] = value
    return merge_dict


def exporter_log(file_path, exporter, name=None, user=None):
    from django_exporter.models import ExporterHistory
    exporter = exporter.__name__
    return ExporterHistory.create(file_path, exporter, name=name, user=user)


def covert_dict_object(data):
    return DictObject(**data)
