from django.contrib import admin

from django_exporter.models import ExporterHistory


class ExporterHistoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'user', 'exporter', 'file', 'status', 'end_date', 'created_date',)
    readonly_fields = ('created_date',)
    list_filter = ('status', 'created_date', 'exporter', 'end_date')
    search_fields = ('name', 'file')
    raw_id_fields = ('user',)


admin.site.register(ExporterHistory, ExporterHistoryAdmin)
