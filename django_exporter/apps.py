from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class ExporterConfig(AppConfig):
    name = 'django_exporter'
    verbose_name = _("Django exporter")
