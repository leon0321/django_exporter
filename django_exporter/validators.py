import json

from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


def validate_json(value):
    if not value:
        return
    try:
        json.loads(value)
    except:
        raise ValidationError(_('Not valid Json'))
