from djchoices import DjangoChoices, ChoiceItem
from django.utils.translation import gettext_lazy as _


class ReportStatus(DjangoChoices):
    REQUESTED = ChoiceItem(1, label=_('Requested'))
    IN_PROGRESS = ChoiceItem(2, label=_('In Progress'))
    READY = ChoiceItem(3, label=_('Ready to Download'))
    NOT_AVAILABLE = ChoiceItem(4, label=_('Not Available'))
