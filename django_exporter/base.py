# -*- coding: utf-8 -*-
import os
from inspect import ismethod

from django.core.exceptions import FieldDoesNotExist

from django_exporter.utilities import get_dict_fields
import xlsxwriter


class BaseExporter(object):
    file_name = None
    worksheet_name = None
    fields = {}
    formats = {}
    model = None
    number_column_title = 0
    start_row = 0
    current_row = 0

    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        file_name = self.model.__class__.__name__ + '.xlsx'
        ws_name = file_name
        self.file_name = kwargs.get('file_name', file_name if self.file_name is None else self.file_name)
        self.worksheet_name = kwargs.get('worksheet_name',
                                         ws_name if self.worksheet_name is None else self.worksheet_name)
        self.writer = self.get_writer()
        self.columns_fields, self.columns_titles = self.convert_format()
        if kwargs.get('query_kwargs'):
            self.queryset = self.get_queryset(**kwargs.get('query_kwargs'))
        else:
            self.queryset = self.get_queryset()
        self.worksheet = self.get_worksheet()
        self.formats_titles, self.formats_fields = self.get_formats()

    def get_queryset(self, **kwargs):
        if kwargs:
            return self.model.objects.filter(**kwargs)
        return self.model.objects.all()

    def get_formats(self):
        config_format = self.create_conf_format_cell()
        formats_title, formats_field = {}, {}
        for field_name in self.fields:
            name_format_title = self.fields.get(field_name).get('format', {}).get('t')
            name_format_field = self.fields.get(field_name).get('format', {}).get('c')
            if name_format_title:
                formats_title[field_name] = config_format.get(name_format_title)
            if name_format_field:
                formats_field[field_name] = config_format.get(name_format_field)
        return formats_title, formats_field

    def update_formats(self):
        for field_name in self.fields:
            self.formats[field_name] = self.format_field(field_name)
        return self.formats

    def create_conf_format_cell(self):
        return {}

    def convert_format(self):
        columns_fields = {}
        columns_titles = {}
        for key, dic in self.fields.items():
            if not dic.get('column', None):
                raise ValueError('key column is no present in [{0}:{1}]'.format(key, dic))
            columns_fields.update({key: dic.get('column')})
            title = self.get_title(key, dic.get('title', None))
            columns_titles.update({key: u'{}'.format(title)})
        columns_fields = get_dict_fields(columns_fields)
        return columns_fields, columns_titles

    def get_title(self, field_name, title):
        if title:
            return title
        try:
            model_field = self.model._meta.get_field(field_name)
            title = '{}'.format(model_field.verbose_name)
        except FieldDoesNotExist:
            title = field_name.capitalize().replace('__', ' ').replace('_', ' ')
        return title

    def get_writer(self):
        if not getattr(self, 'writer', None):
            dirname = os.path.dirname(self.file_name)
            if not os.path.exists(dirname):
                os.makedirs(dirname)
            self.writer = xlsxwriter.Workbook(self.file_name)
        return self.writer

    def get_worksheet(self):
        if not getattr(self, 'worksheet', None):
            self.worksheet = self.get_writer().add_worksheet(name=self.worksheet_name)
        return self.worksheet

    def get_value_field(self, field_name, instance):
        try:
            get_value_field_method = getattr(self, 'get_value_{}'.format(field_name))
            return get_value_field_method(instance, field_name)
        except AttributeError:
            return self.get_value_field_or_method(instance, field_name)

    def format_field(self, field_name):
        f = {}
        if hasattr(self, 'format_{}'.format(field_name)):
            method = getattr(self, 'format_{}'.format(field_name))
            return method()
        if not self.fields.get('format', None):
            return
        if self.fields.get('format').get('t', None):
            f['title'] = self.fields.get('format').get('t')
        if self.fields.get('format').get('c', None):
            f['column'] = self.fields.get('format').get('c')
        if f:
            return f

    def clean_field(self, field_name, value):
        if hasattr(self, 'clean_{}'.format(field_name)):
            method = getattr(self, 'clean_{}'.format(field_name))
            return method(field_name, value)
        return value

    def convert_instance_to_data(self, instance, empty=False):
        data = {}
        for field_name in self.fields:
            if empty:
                value_cleaned = None
            else:
                value = self.get_value_field(field_name, instance)
                value_cleaned = self.clean_field(field_name, value)
            data.update({field_name: value_cleaned})
        return data

    def get_data_rows(self):
        data_rows = []
        self.current_row = 0
        for index, instance in enumerate(self.queryset):
            self.current_row = index + self.start_row + 2
            data_rows.append(self.convert_instance_to_data(instance))
        return data_rows

    def set_column(self, field_name, value):
        if not value:
            return
        number_chars = len(max(u'{}'.format(value).split('\n'), key=len)) + 2
        column = self.columns_fields.get(field_name)
        self.worksheet.set_column(column, column, number_chars)

    def set_columns(self, data_first_row):
        for field_name, value in data_first_row.items():
            self.set_column(field_name, value)

    def write_field(self, field_name, value, row, col=None):
        if self.number_column_title == row:
            cell_format = self.formats_titles.get(field_name)
        else:
            cell_format = self.formats_fields.get(field_name)
        self.worksheet.write(row + self.start_row, self.columns_fields.get(field_name), value, cell_format)

    def write_row(self, data, i):
        for field_name, value in data.items():
            self.write_field(field_name, value, i)

    def write_rows(self, data_rows):
        data_rows.insert(self.number_column_title, self.columns_titles)
        for row, data in enumerate(data_rows):
            self.write_row(data, row)
        self.set_columns(self.columns_titles)

    def start_export(self, *args, **kwargs):
        data_rows = self.get_data_rows()
        self.write_rows(data_rows)
        self.writer.close()
        return self.file_name

    @classmethod
    def get_value_field_or_method(cls, instance, field_name):
        list_names = field_name.split('__')
        final_name = list_names[-1]
        current_instance = instance
        for name in list_names:
            value = getattr(current_instance, name, None)
            if value == 0 or value == 0.0:
                return value
            if not value:
                return
            if ismethod(value):
                value = value()
            if final_name == name:
                return value
            current_instance = value
