import json
import os
from datetime import datetime

from django.core.files.base import File
from django.db import models

from django.utils.translation import gettext_lazy as _

from django_exporter.choices import ReportStatus
from django_exporter.validators import validate_json


class ExporterHistory(models.Model):
    name = models.CharField(_('name'), max_length=255)
    user = models.ForeignKey('auth.User', verbose_name=_('user'), null=True, on_delete=False)
    json_data = models.TextField(_('json data'), null=True, validators=[validate_json])
    file = models.FileField(_('file'), upload_to='exporter_history', max_length=500)
    created_date = models.DateTimeField(_('created date'), auto_now_add=True)
    end_date = models.DateTimeField(_('end date'), null=True, blank=True)
    exporter = models.CharField(_('exporter'), max_length=255)
    status = models.PositiveIntegerField(_('status'), choices=ReportStatus.choices, default=ReportStatus.REQUESTED)

    def __str__(self):
        return u'{}'.format(self.name)

    @property
    def data(self):
        if not self.json_data:
            return {}
        return json.loads(self.json_data)

    @data.setter
    def data(self, json_data):
        self.json_data = json.dumps(json_data or {})

    @staticmethod
    def create(file_path, exporter, name=None, user=None):
        if not name:
            name = os.path.basename(file_path).split('.')[0]
        exporter = ExporterHistory.objects.create(**{
            'file': file_path,
            'name': name,
            'user': user,
            'exporter': exporter,
        })
        exporter.file.save(os.path.basename(file_path), File(open(file_path, 'rb')))
        exporter.file.close()
        os.remove(file_path)
        return exporter

    def save_file(self, file_path):
        self.file.save(os.path.basename(file_path), File(open(file_path, 'rb')))
        self.file.close()
        self.end_date = datetime.now()
        self.status = ReportStatus.READY
        os.remove(file_path)
        self.save()

    def update_status(self, status):
        self.status = status
        self.save()
