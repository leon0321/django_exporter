from django.contrib import admin

# Register your models here.
from myapp.models import ModelA, ModelB


class ModelBInline(admin.TabularInline):
    model = ModelB
    extra = 0


class ModelAAdmin(admin.ModelAdmin):
    inlines = [ModelBInline]
    list_display = ('id', 'name', 'description', 'price')


admin.site.register(ModelA, ModelAAdmin)
