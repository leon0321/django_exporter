import mimetypes, os
from wsgiref.util import FileWrapper


# Function download file
def download_file(file_path, name_download_file=None):
    name_download_file = os.path.basename(name_download_file if name_download_file else file_path)
    from django.http import HttpResponse
    fixed_file_wrapper = FileWrapper(open(file_path, 'rb'))
    response = HttpResponse(fixed_file_wrapper, content_type=mimetypes.guess_type(file_path)[0])
    response['Content-Length'] = os.path.getsize(file_path)
    response['Content-Disposition'] = "attachment; filename=%s" % name_download_file
    return response