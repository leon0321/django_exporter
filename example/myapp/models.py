# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from datetime import datetime


class ModelA(models.Model):
    name = models.CharField(max_length=144)
    description = models.TextField()
    price = models.FloatField(default=1.0)

    def get_now(self):
        return datetime.today()


class ModelB(models.Model):
    items = models.IntegerField(default=0)
    code = models.CharField(max_length=8)
    user = models.ForeignKey('auth.User')
    model_a = models.ForeignKey(ModelA)
    date = models.DateField()

    def concat_item_and_code(self):
        return u'{}-{}'.format(self.items, self.code)
