# -*- coding: utf-8 -*-
from django.utils import timezone
from django_exporter.base import BaseExporter
import os
from django.conf import settings
from myapp.models import ModelB


class ExporterExample(BaseExporter):
    file_name = os.path.join(settings.MEDIA_ROOT, 'example_report.xlsx')
    fields = {
        # model B
        'id': {'column': 'A', 'title': u'ID'},
        'code': {'column': 'B', 'title': u'CODE', 'format': {'t': 'red'}},
        'items': {'column': 'C', 'title': u'#ITEMS', 'format': {'t': 'red', 'c': 'blue'}},
        'concat_item_and_code': {'column': 'N', 'title': u'HASH', 'format': {'t': 'red', 'c': 'green'}},
        'date': {'column': 'D', 'title': u'DATE', 'format': {'t': 'green', 'c': 'date'}},
        # user
        'user__username': {'column': 'E', 'title': u'USERNAME', 'format': {'t': 'red', 'c': 'font'}},
        'user__email': {'column': 'F', 'title': u'E-MAIL', 'format': {'t': 'red', 'c': 'font'}},
        'user__get_full_name': {'column': 'G', 'title': u'FULL NAME', 'format': {'t': 'blue', 'c': 'font'}},
        # model a
        'model_a__name': {'column': 'H', 'title': u'MODEL A NAME', 'format': {'t': 'red', 'c': 'font'}},
        'model_a__description': {'column': 'J', 'title': u'DESCRIPTION', 'format': {'t': 'red', 'c': 'font'}},
        'model_a__get_now': {'column': 'I', 'title': u'TIME', 'format': {'t': 'red', 'c': 'font'}},
        'model_a__price': {'column': 'K', 'title': u'TIME', 'format': {'t': 'green', 'c': 'number'}},
        # custom methods
        'custom_method_1': {'column': 'L', 'title': u'CUSTOM METHOD 1', 'format': {'t': 'blue', 'c': 'font'}},
        'custom_method_2': {'column': 'M', 'title': u'CUSTOM METHOD 2', 'format': {'t': 'red', 'c': 'font'}},
    }
    model = ModelB

    def get_value_custom_method_1(self, instance, field_name):
        return timezone.now().strftime('%Y.%m.%d')

    def get_value_custom_method_2(self, instance, field_name):
        return u'{} - {}'.format(timezone.now().strftime('%Y.%m.%d'), instance.code)

    def create_conf_format_cell(self):
        """
        This method creates formats for cells that are used by the exported
        for more information about formats visit http://xlsxwriter.readthedocs.io
        :return a dict with the formats:
        """
        font_col = {'font_name': 'Arial', 'align': 'center'}
        red_format = {'fg_color': 'red', 'font_color': 'white'}
        blue_format = {'fg_color': 'blue', 'font_color': 'green'}
        green_format = {'fg_color': 'green', 'font_color': 'black'}
        format_date = {'num_format': 'm/d/yy', 'font_name': 'Arial', 'align': 'center'}
        format_number = {'num_format': '#,##0.00'}

        green = self.writer.add_format(green_format)

        red = self.writer.add_format(red_format)

        blue = self.writer.add_format(blue_format)

        font = self.writer.add_format(font_col)

        date = self.writer.add_format(format_date)

        number = self.writer.add_format(format_number)

        return {
            'green': green,
            'red': red,
            'blue': blue,
            'font': font,
            'date': date,
            'number': number,
        }
