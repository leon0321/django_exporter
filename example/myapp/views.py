from django.views.generic.base import View

from myapp.exporters import ExporterExample
from myapp.utilities import download_file


class ExportInfoView(View):
    def get(self, request, *args, **kwargs):
        # query_kwargs is a dict to filter query example: ExporterExample(query_kwargs={'name':'air'})
        exporter = ExporterExample(query_kwargs={})
        path_file_exported = exporter.start_export()
        return download_file(path_file_exported)