import os
import sys
from setuptools import setup, find_packages

version = '0.6.1'

if sys.argv[-1] == 'publish':  # upload to pypi
    os.system("python setup.py register sdist bdist_egg bdist_wheel upload")
    print("You probably want to also tag the version now:")
    print("  git tag -a %s -m 'version %s'" % (version, version))
    print("  git push --tags")
    sys.exit()

setup(
    name='django_exporter',
    version=version,
    license='GPL',

    install_requires=[
        'django>=1.7.0,<2.3',
        'XlsxWriter>=0.7.3,<=1.1.1',
        'django-choices<=1.6.1',
    ],

    description='Data exporter in files xlsx',
    long_description=open('README.rst').read(),

    author='Leonardo Ortega Hernandez',
    author_email='leon9894@gmail.com',

    url='https://bitbucket.org/leon0321/django_exporter',
    download_url='https://bitbucket.org/leon0321/django_exporter/zipball/master',
    packages=find_packages(),
    include_package_data=True,

    zip_safe=False,
    classifiers=[
        'Development Status :: 5 - Devel/Test',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Framework :: Django',
    ]
)
